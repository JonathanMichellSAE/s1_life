﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public AudioClip mainSong;
    public AudioClip transitionClip;
    public AudioSource audioSource;

    public static AudioManager instance;

    void Awake()
    {
        if (AudioManager.instance == null)
        {
            AudioManager.instance = this;
            GameObject.DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
    }
    private void Start()
    {
        audioSource.clip = mainSong;
        audioSource.Play();
    }

    public void PlayAudioClip()
    {
        //audioSource.clip = transitionClip;
        audioSource.PlayOneShot(transitionClip);
        //Debug.Log("Played the audio clip");
    }

}
