﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider2D))]
public class OnTriggerLoadLevel : MonoBehaviour
{
    public PlayerMovement playerMove;
    public AudioManager audioManager;
    public float distanceToSlowMoveSpeed = 0.75f;

    public Image white;
    public Animator anim;
    
    public string levelToLoad; // Public variable for next level to load

    Transform playerPos;
    Transform transitionBoundaryPos;

    public float distanceBetween; // The distance between the player and desired y pos

    private Coroutine coroutine;

    //public Vector2 transitionYPos; // The desired y pos for transitions to occur

    void Start()
    {
        //audioManager = GameObject.FindObjectOfType(audioManager.transitionClip);
        
        // Set hasMadeTransition bool in AudioManager to false 

        playerPos = GameObject.FindGameObjectWithTag("Player").transform; // Finds player and returns player pos
        playerMove = playerPos.GetComponent<PlayerMovement>();

        transitionBoundaryPos = this.transform; // Will this work?
    }

    void Update()
    {
        distanceBetween = ((playerPos.transform.position.y) - (transitionBoundaryPos.transform.position.y));
        //distanceBetween = (Mathf.Abs(playerPos.transform.position.y) - Mathf.Abs(transitionPos.transform.position.y));

        if (Mathf.Abs(distanceBetween) <= distanceToSlowMoveSpeed) // If the absoulte distance between playerPos and transitionYPos less than 0.05
        {
            playerMove.moveSpeedBeingSlowed = true;
        }
        else
        {
            playerMove.moveSpeedBeingSlowed = false;
        }

        if (Mathf.Abs(distanceBetween) <= 0.1 && coroutine == null) // If the absoulte distance between playerPos and transitionYPos less than 0.05
        {
            //Debug.Log("The distanceBetween is less than 0.1");
            coroutine = StartCoroutine(Fading());
        }
    }

    IEnumerator Fading()
    {
        AudioManager.instance.PlayAudioClip();

        anim.SetBool("Fade", true); // Sets fade parameter to true and starts fade out animation
        yield return new WaitUntil(() => white.color.a == 1); // Waits until alpha value is 1

        // Set hasMadeTransition bool in AudioManager to true 
        //AudioManager.instance.PlayAudioClip();

        SceneManager.LoadScene(levelToLoad); // Loads the next level
    }

    /*
     * Get reference to centre of player current pos and centre of transition lines pos
     * Check distance between player and transition lines
     * If distance is < 0.1 then trigger transition
     */

    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    Debug.Log("Collision detected");
    //    //SceneManager.LoadScene(levelToLoad);
    //}
}
