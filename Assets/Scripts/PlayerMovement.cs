﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public OnTriggerLoadLevel onTrigLoadLev;
    
    //public PlayerControls playerControls;
    //Vector3 vectorToMove;

    public float idleTimer;
    public float moveSpeed = 2f;
    public Rigidbody2D rb;
    
    Vector2 movement;

    private bool isMoving = false;
    public bool moveSpeedBeingSlowed = false;
    public float reduceSpeedMultiplier = 0.5f;

    void Start()
    {
        idleTimer = 0f; // Sets timer to 0 on start
    }

    void Update()
    {
        //transform.Translate(Move, Time.deltaTime * moveSpeed);

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if (movement == Vector2.zero) // If no inputs are detected
        {
            isMoving = false; // Sets isMoving bool to false
            idleTimer += Time.deltaTime; // Starts timer
        }
        else // Else inputs are detected
        {
            isMoving = true; // Sets isMoving bool to true
            idleTimer = 0f; // Sets timer to 0
        }
    }

    private void FixedUpdate()
    {
        if(moveSpeedBeingSlowed == false)
        {
            rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        }
        else
        {
            rb.MovePosition(rb.position + movement * (moveSpeed*reduceSpeedMultiplier) * Time.fixedDeltaTime);
        }
    }
}
