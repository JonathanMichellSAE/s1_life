﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuitGame : MonoBehaviour
{
    public PlayerMovement playerMoving;
    public GameObject quitButton;

    private void Start()
    {
        playerMoving = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();

        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        if(sceneName == "Painting 01.1")
        {
            // Show quit button
            quitButton.SetActive(true);
        }
    }

    private void Update()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        if (playerMoving.idleTimer > 15.0f)
        {
            // Show quit button
            quitButton.SetActive(true);
        }
        else if (sceneName != "Painting 01.1")
        {
            // Hide the quit button
            quitButton.SetActive(false);
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
            Debug.Log("Escape key pressed, game is exiting");
        }
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Game is exiting");
    }

}
