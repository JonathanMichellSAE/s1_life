﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionEffects : MonoBehaviour
{
    Camera MainCamera;

    //int normalZoom = 50;
    public int zoomAmount = 10;
    public float zoomTime = 5;

    // bool isTouchingTransitionBoundary = false;

    private void Start()
    {
        MainCamera = Camera.main;
    }

    private void Update()
    {
        // If player is touching a transition boundary, isTouchingTransitionBoundary = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        // Debug.Log("Ready to Zooooom");
        MainCamera.fieldOfView = Mathf.Lerp(MainCamera.fieldOfView, zoomAmount, Time.deltaTime * zoomTime);

        //GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, zoomAmount, Time.deltaTime * zoomTime);
    }
}